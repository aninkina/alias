package com.example.alias.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface WordsDao {

    @Query("SELECT * FROM words_en ORDER BY RANDOM() LIMIT 1")
    suspend fun getRandomEnglishWord(): WordEnglishEntity

    @Query("SELECT * FROM words_ge ORDER BY RANDOM() LIMIT 1")
    suspend fun getRandomGeorgianWord(): WordGeorgianEntity

    @Query("SELECT * FROM words_ru WHERE topic = :topic ORDER BY RANDOM() LIMIT 1")
    suspend fun getRandomRussianWord(topic: String): WordRussianEntity
//
//    @Query("SELECT * FROM words_ru WHERE key_theme == :theme ORDER BY RANDOM()")
//    suspend fun getRandomRussianWordByTheme(theme: String): WordRussianEntity

    @Query("SELECT * FROM words_ru")
    suspend fun getAllRussianWords(): List<WordRussianEntity>

    @Insert
    suspend fun insertRusWord(rusEntity: WordRussianEntity)

    @Query("DELETE FROM words_ru WHERE keyword = :keyword AND topic = :topic")
    fun deleteByKeyword(keyword: String, topic: String)
}
