package com.example.alias.ui.configure.dictionary

data class WordDO (
    val word: String,
    val topic: String,
)
