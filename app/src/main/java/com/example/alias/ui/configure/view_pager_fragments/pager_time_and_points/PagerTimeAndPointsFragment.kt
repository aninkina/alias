package com.example.alias.ui.configure.view_pager_fragments.pager_time_and_points

import android.content.Context
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.alias.R
import com.example.alias.databinding.FragmentPagerTimeAndPointsBinding
import com.example.alias.room.WordsDatabase
import com.example.alias.ui.configure.view_pager_fragments.base.BaseFragment
import com.example.alias.ui.configure.view_pager_fragments.pager_time_and_points.topic.TopicAdapter
import com.example.alias.ui.configure.vm.ConfigureViewModel
import com.example.alias.util.GameMode
import me.tankery.lib.circularseekbar.CircularSeekBar

class PagerTimeAndPointsFragment :
    BaseFragment<FragmentPagerTimeAndPointsBinding>(FragmentPagerTimeAndPointsBinding::inflate),
    TopicAdapter.ListItemClickListener  {

    private var viewModel: ConfigureViewModel? = null

//    private val viewModel: ConfigureViewModel by viewModels(
//        ownerProducer = { requireParentFragment() }
//    )
    private val adapter = TopicAdapter(this)

    private var isToastHandled = false

    override fun init() {
        viewModel = ConfigureViewModel.getInstance(context!!.applicationContext)
        viewModel!!.setIsClassic(true)

        initTimeSeekBar()
        initPointSeekbar()
        initOnClickListeners()
        initObservers()
        initRecycler()
        observeLD()
        viewModel!!.getAllTopics()
    }
    private fun setUI(data: List<String>?) {
        data?.let {
            setTopic(it[0])
            adapter.items = it as MutableList<String>
            adapter.notifyDataSetChanged()
            markTopic(0)
        }
    }

    private fun markTopic(index:Int){
        adapter.lastMarkedTopicIndex = adapter.markedTopicIndex
        adapter.markedTopicIndex = index
        adapter.notifyDataSetChanged()
    }

    private fun observeLD() {
        viewModel?.topicLiveData?.observe(viewLifecycleOwner) {
            setUI(it)
        }

        viewModel?.topicIndex?.observe(viewLifecycleOwner) {
            markTopic(it)
        }
    }

    private fun initRecycler() {
        binding.rvTopic.adapter = adapter
        binding.rvTopic.layoutManager = LinearLayoutManager(context)
    }

    private fun initPointSeekbar() {
        val pointSeekBar = binding.pointSeekBar
        val pointTV = binding.pointTV
        pointSeekBar.progress =
            (DEFAULT_POINTS_TO_WIN).toFloat()
        pointTV.text = "15"
        viewModel!!.setPointsToWin(DEFAULT_POINTS_TO_WIN)
        pointSeekBar.max = 25F;
        pointSeekBar.setOnSeekBarChangeListener(
            object : CircularSeekBar.OnCircularSeekBarChangeListener {
                override fun onProgressChanged(
                    circularSeekBar: CircularSeekBar?,
                    progress: Float,
                    fromUser: Boolean
                ) {
                    val progressToShow = progress - (progress % 5)
                    pointSeekBar.progress = progressToShow
                    showProgressOnTV(progressToShow.toInt())
                }

                override fun onStartTrackingTouch(seekBar: CircularSeekBar?) {}

                override fun onStopTrackingTouch(seekBar: CircularSeekBar?) {
                    if (seekBar != null) {
                        if (seekBar.progress.toInt() < 5) {
                            seekBar.progress = 5F
                            showProgressOnTV(5)
                        }
                        viewModel!!.setPointsToWin(pointTV.text.toString().toInt())
                    }
                }

                fun showProgressOnTV(progress: Int) {
                    pointTV.text = progress.toString()
                }
            }
        )
    }

    private fun initTimeSeekBar() {
        val timeSeekBar = binding.timeSeekBar
        val timeTV = binding.timeTV
        timeSeekBar.progress =
            (DEFAULT_TIME_PER_ROUND).toFloat()
//        timeTV.text = timeSeekBar.progress.toInt().toString()
        timeTV.text = "60"
        viewModel?.setTimePerRound(DEFAULT_TIME_PER_ROUND)
        timeSeekBar.setOnSeekBarChangeListener(
            object : CircularSeekBar.OnCircularSeekBarChangeListener {
                override fun onProgressChanged(
                    circularSeekBar: CircularSeekBar?,
                    progress: Float,
                    fromUser: Boolean
                ) {
                    val progressToShow = progress - (progress % 5)
                    timeSeekBar.progress = progressToShow
                    showProgressOnTV(progressToShow.toInt())
                }

                override fun onStartTrackingTouch(seekBar: CircularSeekBar?) {}

                override fun onStopTrackingTouch(seekBar: CircularSeekBar?) {
                    if (seekBar != null) {
                        if (seekBar.progress.toInt() < 5) {
                            seekBar.progress = 5F
                            showProgressOnTV(5)
                        }
                        viewModel!!.setTimePerRound(timeTV.text.toString().toInt())
                    }
                }

                fun showProgressOnTV(progress: Int) {
                    timeTV.text = progress.toString()
                }
            }
        )
    }

    private fun initObservers() {
        viewModel?.gameMode?.observe(viewLifecycleOwner) {
            if (it.timePerRound != null && it.pointsToWin != null && !isToastHandled) {
                if (it.isClassic == null) {
                    makeToastMessage(getString(R.string.choose_game_mode))
                    isToastHandled = true
                }
            }

            it.isClassic?.let { isClassic ->
                if (isClassic) {
                    binding.btnClassic.setBackgroundResource(R.drawable.game_mode_chosen_btn_shape_inversed)
                } else {
                    binding.btnClassic.setBackgroundResource(R.drawable.game_mode_btn_shape_inversed)
                }
            }
        }
    }

    private fun initOnClickListeners() = with(binding) {
        btnClassic.setOnClickListener { viewModel?.setIsClassic(true) }
    }

    companion object {
        private const val DEFAULT_TIME_PER_ROUND = 60
        private const val DEFAULT_POINTS_TO_WIN = 15
    }

    override fun onListItemClick(clickedItemIndex: Int) {
        if(viewModel?.topicLiveData?.value != null) {
            viewModel?.setTopicIndex(clickedItemIndex)
            setTopic(viewModel?.topicLiveData!!.value!![clickedItemIndex])
        }
    }

    private fun setTopic(topic: String) {
        val sharedPreference =
            requireActivity().getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        val editor = sharedPreference.edit()
        editor.putString("topic",topic)
        editor.commit()
    }
}
