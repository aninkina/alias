package com.example.alias.ui.classic

import android.content.Context
import android.media.MediaPlayer
import android.os.CountDownTimer
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.LiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import com.example.alias.R
import com.example.alias.databinding.ClassicFragmentBinding
import com.example.alias.di.viewModels
import com.example.alias.extensions.safeNavigate
import com.example.alias.ui.base.BaseFragment
import com.example.alias.ui.classic.vm.ClassicViewModel
import com.example.alias.util.GameMode
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules

class ClassicFragment : BaseFragment<ClassicFragmentBinding>(ClassicFragmentBinding::inflate) {

    private lateinit var countDownTimer: CountDownTimer
    private lateinit var gameMode: GameMode
    private lateinit var mediaPlayerThreeSeconds: MediaPlayer
    private lateinit var mediaPlayerTenSeconds: MediaPlayer

    private val textRU = mutableListOf(
        "Объясняйте при помощи синонимов (однокоренные нельзя!)",
        "Объясни слово рисунком, не используй жестикуляцию!",
        "Крокодил! Объясни слово жестами.",
        "Прочитайте загаданное слово задом наперед (не больше 3 попыток)"
    );
    private var isGameFinished = false
    private var gotToWinningPoints = false
    private var isBonusRound = true
    private var isStartNextTeamRequired = false

    private val safeArgs: ClassicFragmentArgs by navArgs()
    private val classicViewModel: ClassicViewModel by viewModel()

    private var timePerRound = 0
    private var prevCompletionPoints = 0

    private val language = fun(context: Context) =
        (context.getSharedPreferences("languageSharedPreference", Context.MODE_PRIVATE))
            .getInt("language", 0)

    override fun init() {
        loadKoinModules(viewModels)
        initTopic()
        initMediaPlayer()
        initArrowBtn()
        initGameMode()
        initObservers()
        classicViewModel.wordLiveData.observe(viewLifecycleOwner) {
            binding.btnWord.text = it
        }
        classicViewModel.getRandomWord(binding.tvTopic.text.toString())
        binding.btnWord.setOnClickListener {
            classicViewModel.incrementScore()
            classicViewModel.getRandomWord(binding.tvTopic.text.toString())
        }

        initCountDown(timePerRound)
        startNextTeamRound()
        onBackPressed()
    }

    private fun initTopic(){
        val sharedPreference =
            requireActivity().getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        binding.tvTopic.text = sharedPreference.getString("topic", "NULL")
    }

    private fun initMediaPlayer() {
        mediaPlayerThreeSeconds = MediaPlayer.create(requireContext(), R.raw.countdown)
        mediaPlayerTenSeconds = MediaPlayer.create(requireContext(), R.raw.ten_seconds_left)
    }

    override fun onStart() {
        super.onStart()
        if (binding.tvCountDown.text == "0") {
            handleIsGameFinished()
            classicViewModel.saveCurrentTeamScore()
            handleGameContinuation()
        }
    }

    private fun onBackPressed() {
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                navigateToEnsureDialogFragment()
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        countDownTimer.cancel()
        unloadKoinModules(viewModels)
        mediaPlayerThreeSeconds.release()
        mediaPlayerTenSeconds.release()
    }

    private fun initArrowBtn() {
        binding.btnShowScore.setText(getString(R.string.score))
        binding.btnShowScore.setDrawable(R.drawable.ic_arrow_up)
        binding.btnShowScore.setOnClickListener {
            if (isStartNextTeamRequired)
                classicViewModel.toggleIsNextTurn()
            else
                navigateToScoreBreak(isStartNextTeamRequired)
        }
    }

    private fun initCountDown(timePerRound: Int) {
        countDownTimer = object : CountDownTimer(timePerRound * 1000L, 1000) {
            override fun onTick(timeLeft: Long) {
                val toDisplay = timeLeft / 1000
                binding.tvCountDown.text = toDisplay.toString()
                when (toDisplay) {
                    3L -> mediaPlayerThreeSeconds.start()
                    10L -> mediaPlayerTenSeconds.start()
                }
            }

            override fun onFinish() {
                isStartNextTeamRequired = true
                startNextTeamRequired()
                handleGameResumption()
            }
        }
        countDownTimer.start()
    }

    private fun startNextTeamRequired() {
        if (isStartNextTeamRequired) {
            with(binding) {
                btnShowScore.setText(getString(R.string.continue_txt))
                btnShowScore.setDrawable(R.drawable.ic_arrow_right)
                btnShowScore.setBtnColor(R.drawable.green_circle_btn_shape)
                btnWord.isClickable = false
            }
        } else {
            with(binding) {
                btnShowScore.setText(getString(R.string.score))
                btnShowScore.setDrawable(R.drawable.ic_arrow_up)
                btnShowScore.setBtnColor(R.drawable.circle_button_shape)
                btnWord.isClickable = true
            }
        }
    }

    private fun handleGameResumption() {
        if (classicViewModel.hasBackPressed.value!! != 1) {
            handleIsGameFinished()
            classicViewModel.saveCurrentTeamScore()
            handleGameContinuation()
        }
    }

    private fun handleIsGameFinished() {
        isGameFinished =
            gotToWinningPoints &&
                    classicViewModel.teamPointer == classicViewModel.currentTeams.size - 1
        val poinnter =classicViewModel.teamPointer
    }

    private fun handleGameContinuation() = when {
        !isGameFinished -> navigateToScoreBreak(isStartNextTeamRequired)
        !isBonusRound -> {
            isBonusRound = true
            val leftForBonus =
                classicViewModel.currentTeams.filter { it.value >= gameMode.pointsToWin!! }

            if (leftForBonus.size <= 1)
                navigateToResultFragment()
            else {
                classicViewModel.setTeams(leftForBonus, isBonusRound)
                navigateToScoreBreak(isStartNextTeamRequired)
            }
        }
        else -> {
            val leftForBonus =
                classicViewModel.currentTeams.filter { it.value == classicViewModel.currentTeams.values.maxOrNull() }
            if (leftForBonus.size <= 1)
                navigateToResultFragment()
            else {
                classicViewModel.setTeams(leftForBonus, isBonusRound)
                navigateToScoreBreak(isStartNextTeamRequired)
            }
        }
    }


    private fun navigateToResultFragment() {
        val action = ClassicFragmentDirections.actionClassicFragmentToResultFragment(
            gameMode,
            classicViewModel.teamsTotal.values.toIntArray()
        )
        findNavController().safeNavigate(action)
    }

    private fun startNextTeamRound() {
        classicViewModel.startNextTeamRound()
        isStartNextTeamRequired = false
        startNextTeamRequired()
        prevCompletionPoints = 0
        makeRequest()
        binding.tvCurrentTeam.text = classicViewModel.currentTeam
        countDownTimer.cancel()
        countDownTimer.start()
    }

    private fun navigateToScoreBreak(isStartNextTeamRequired: Boolean) {
        findNavController().safeNavigate(
            ClassicFragmentDirections.actionClassicFragmentToScoreBreakFragment(
                true,
                isStartNextTeamRequired
            )
        )
    }


//    private fun initRecycler() {
//        recyclerView = binding.classicFragmentRecycler
//        recyclerView.adapter = wordsAdapter
//        recyclerView.layoutManager = LinearLayoutManager(requireContext())
//    }

    private val wordCount: Int = 1

    private fun initObservers() {
        classicViewModel.hasBackPressed.observe {
            if (it >= 2) {
                lifecycleScope.launch {
                    delay(classicViewModel.dismissDuration + 40)
                    if (binding.tvCountDown.text == "0" && it == 2 || it == 3) {
                        isStartNextTeamRequired = true
                        startNextTeamRequired()
                        countDownTimer.cancel()
                        handleGameResumption()
                    }
                    classicViewModel.handleBackPress(0)
                }
            }
        }

        classicViewModel.currentScore.observe {
            if (it >= gameMode.pointsToWin!!)
                gotToWinningPoints = true

            if (
                it > 0 && (it - (classicViewModel
                    .currentTeams[classicViewModel.currentTeam]
                    ?: 0)) - wordCount == prevCompletionPoints
            ) {
                classicViewModel.switchHasCompleted()
                prevCompletionPoints += wordCount
            }

            binding.tvCurrentScore.text = it.toString()
        }

        classicViewModel.hasCompleted.observe {
            if (it) {
                makeRequest()
                classicViewModel.switchHasCompleted()
            }
        }

        classicViewModel.isNextTurn.observe {
            if (it) {
                startNextTeamRound()
                classicViewModel.toggleIsNextTurn()
            }
        }
    }


    private fun initGameMode() {
        gameMode = safeArgs.gameMode
        gameMode.pointsToWin = gameMode.pointsToWin ?: 90
        val map = (gameMode.teams ?: listOf("Teams1", "Teams2")).associateWith { 0 }.toMutableMap()
        classicViewModel.setTeams(map)
        timePerRound = gameMode.timePerRound ?: 30
        classicViewModel.gameMode = gameMode
    }


    private fun makeRequest() = viewLifecycleOwner.lifecycleScope.launch {
//        wordsAdapter.setData(getWords())
        val rndIndex = (0..3).random()
        if (language(requireContext()) == 1) {
            binding.tvRules.text = textRU[rndIndex]
        } else {
            binding.tvRules.text = textRU[rndIndex];
        }
    }

    private fun <T> LiveData<T>.observe(f: (T) -> Unit) = this.observe(viewLifecycleOwner) { f(it) }

    private fun navigateToEnsureDialogFragment() {
        classicViewModel.handleBackPress(1)
        findNavController().safeNavigate(
            ClassicFragmentDirections.actionClassicFragmentToEnsureInExitDialogFragment(
                true
            )
        )
    }

}
