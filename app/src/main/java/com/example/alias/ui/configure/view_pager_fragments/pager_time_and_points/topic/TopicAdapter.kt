package com.example.alias.ui.configure.view_pager_fragments.pager_time_and_points.topic

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.alias.R


class TopicAdapter(private val mOnClickListener: ListItemClickListener) :
    RecyclerView.Adapter<TopicAdapter.ViewHolder>() {

    var lastMarkedTopicIndex : Int? = 0
    var markedTopicIndex : Int? = 0

    var items = mutableListOf<String>()

    interface ListItemClickListener {
        fun onListItemClick(clickedItemIndex: Int)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var clicked : Boolean = false
        val textViewTheme: TextView

        init {
            textViewTheme = view.findViewById(R.id.etTeam)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.topic_item, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.textViewTheme.text = items[position]
        viewHolder.textViewTheme.setOnClickListener{mOnClickListener.onListItemClick(position)}
        if(markedTopicIndex != null && markedTopicIndex == position){
            viewHolder.textViewTheme.background = viewHolder.itemView.getResources().getDrawable(R.drawable.arcade_plus_btn_shape)
        }
        else {
            viewHolder.textViewTheme.background = viewHolder.itemView.getResources().getDrawable(R.drawable.arcade_minus_btn_shape)
        }



    }

    override fun getItemCount() = items.size

}
