package com.example.alias.ui.configure.dictionary.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.alias.R
import com.example.alias.ui.configure.dictionary.WordDO


class DictionaryAdapter(private val mOnClickListener: ListItemClickListener) :
    RecyclerView.Adapter<DictionaryAdapter.ViewHolder>() {

    var items = mutableListOf<WordDO>()

    interface ListItemClickListener {
        fun onListItemClick(clickedItemIndex: Int)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textViewWord: TextView
        val textViewTheme: TextView
        val dictionaryDelete: Button


        init {
            textViewWord = view.findViewById(R.id.dictionaryWord)
            textViewTheme = view.findViewById(R.id.dictionaryTheme)
            dictionaryDelete = view.findViewById(R.id.dictionaryDelete)

        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.dictionary_row_item, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.textViewWord.text = items[position].word
        viewHolder.textViewTheme.text = items[position].topic

        viewHolder.dictionaryDelete.setOnClickListener{mOnClickListener.onListItemClick(position)}
    }

    override fun getItemCount() = items.size

}
