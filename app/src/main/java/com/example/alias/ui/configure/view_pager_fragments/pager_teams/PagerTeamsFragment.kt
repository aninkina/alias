package com.example.alias.ui.configure.view_pager_fragments.pager_teams

import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.alias.R
import com.example.alias.databinding.FragmentPagerTeamsBinding
import com.example.alias.ui.configure.view_pager_fragments.base.BaseFragment
import com.example.alias.ui.configure.view_pager_fragments.pager_teams.adapter.TeamsAdapter
import com.example.alias.ui.configure.vm.ConfigureViewModel

class PagerTeamsFragment :
    BaseFragment<FragmentPagerTeamsBinding>(FragmentPagerTeamsBinding::inflate) {
        private var adapter : TeamsAdapter? = null
//    private val adapter by lazy {
//        TeamsAdapter {
//            viewModel?.setTeams(it)
//        }
//    }
//    private val viewModel: ConfigureViewModel by viewModels(
//        ownerProducer = { requireParentFragment() }
//    )
    private var constraintMessageShown = false

    private var viewModel: ConfigureViewModel? = null


    override fun init() =
        with(binding) {
            println("teams fragment opened")
            viewModel = ConfigureViewModel.getInstance(context!!.applicationContext)
            // Init Recycler
            adapter = TeamsAdapter {
                val vm = viewModel
                viewModel!!.setTeams(it)
            }
            val layoutManager = LinearLayoutManager(requireContext())
            layoutManager.stackFromEnd = true
            recyclerView.adapter = adapter
            recyclerView.layoutManager = layoutManager

            initSwipeListener()
            initObservers()

            btnAddTeam.setOnClickListener {
                if (adapter!!.teams.size < MAX_TEAMS)
                    adapter!!.addTeam()
                else
                    makeToastMessage(getString(R.string.max6Teams))
            }
            btnClassic.setOnClickListener { viewModel!!.setIsClassic(true) }
        }

    private fun initSwipeListener() {
        ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean = false
            override fun getSwipeDirs(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder
            ): Int {
                return if (adapter!!.teams.size > MIN_TEAMS) super.getSwipeDirs(
                    recyclerView,
                    viewHolder
                )
                else 0
            }

            // Delete on Swipe
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.absoluteAdapterPosition
                if (adapter!!.teams.size > MIN_TEAMS)
                    adapter!!.deleteTeam(position)
                else makeToastMessage(getString(R.string.min2Teams))
            }
        }).attachToRecyclerView(binding.recyclerView)
    }

    private fun initObservers() {
        viewModel?.isTeamsInputValid?.observe(viewLifecycleOwner) {
            if (!it && !constraintMessageShown) {
                makeToastMessage(getString(R.string.uniqueConstraint))
                constraintMessageShown = true
            }
        }

        viewModel?.gameMode?.observe(viewLifecycleOwner) { gm ->
            gm.teams?.let {
                if (it.size > 7)
                    binding.drawable.animate().alpha(0f)
                else
                    binding.drawable.animate().alpha(1f)

            }

            gm.isClassic?.let {
                binding.btnClassic.setColor(it)
            }
        }
    }

    companion object {
        private const val MAX_TEAMS = 10
        private const val MIN_TEAMS = 2
    }
}
