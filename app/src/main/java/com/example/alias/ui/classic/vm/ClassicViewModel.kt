package com.example.alias.ui.classic.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.alias.room.WordsDao
import com.example.alias.ui.configure.dictionary.WordDO
import com.example.alias.util.GameMode
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ClassicViewModel(
    private val wordsDao: WordsDao
) : ViewModel() {

    private val wordSet = mutableSetOf<String?>()
    private var _teamsTotal = mutableMapOf<String, Int>()
    private var _currentTeams = mutableMapOf<String, Int>()
    private val _currentScore = MutableLiveData(0)
    private val _hasCompleted = MutableLiveData(false)
    private val _hasBackPressed = MutableLiveData(0)
    var gameMode:GameMode? = null
    private var _currentTeam = "Team1"

    var dismissDuration = 150L

    val hasBackPressed: LiveData<Int>
        get() = _hasBackPressed

    val handleBackPress: (Int) -> Unit = { _hasBackPressed.value = it }

    val currentTeams: Map<String, Int>
        get() = _currentTeams

    val teamsTotal: Map<String, Int>
        get() = _teamsTotal

    private var _teamPointer = 0
    val teamPointer: Int
        get() = _teamPointer

    private val _isNextTurn =
        MutableLiveData(false)

    private val teamsList: List<String>
        get() = this._currentTeams.keys.toList()

    val isNextTurn: LiveData<Boolean>
        get() = _isNextTurn

    val hasCompleted: LiveData<Boolean>
        get() = _hasCompleted

    val currentScore: LiveData<Int>
        get() = _currentScore

    val currentTeam: String
        get() = _currentTeam


    private val _wordLiveData = MutableLiveData<String>()
    var wordLiveData: LiveData<String> = _wordLiveData

    fun toggleIsNextTurn() {
        _isNextTurn.value = !_isNextTurn.value!!
    }

    fun startNextTeamRound() {
        _teamPointer = (_teamPointer + 1) % teamsList.size
        _currentTeam = teamsList[_teamPointer]
        getCurrentTeamScore()
    }

    fun incrementScore() {
        _currentScore.value = _currentScore.value!! + 1
    }

    fun decrementScore() {
        _currentScore.value = _currentScore.value!! - 1
    }

    fun saveCurrentTeamScore() {
        val score = this._currentTeams[currentTeam]
        score?.let {
            this._currentTeams[currentTeam] =
                it + (currentScore.value!! - it)
            _teamsTotal[currentTeam] = currentTeams[currentTeam]!!
        }
    }

    private fun getCurrentTeamScore() {
        _currentScore.value = this._currentTeams[currentTeam] ?: 0
    }

    fun switchHasCompleted() {
        _hasCompleted.value = !_hasCompleted.value!!
    }

    fun setTeams(teams: Map<String, Int>, isBonusRound: Boolean = false) {
        if (!isBonusRound)
            this._teamsTotal = teams.toMutableMap()
        _teamPointer = -1
        this._currentTeams = teams.toMutableMap()
    }

     fun getRandomWord(topic: String) {
        viewModelScope.launch(Dispatchers.IO) {

            kotlin.runCatching {
                wordsDao.getRandomRussianWord(topic)
            }.onSuccess {
                _wordLiveData.postValue(it.name)
            }.onFailure {
                println("Get word by topic smth in getAllWords went wrong ${it.message}")
            }
        }

    }
}
