package com.example.alias.ui.configure.dictionary

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.alias.databinding.FragmentDictionaryBinding
import com.example.alias.ui.configure.dictionary.adapter.DictionaryAdapter
import com.example.alias.ui.configure.view_pager_fragments.base.BaseFragment
import com.example.alias.ui.configure.view_pager_fragments.pager_teams.adapter.TeamsAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel


class DictionaryFragment :
    BaseFragment<FragmentDictionaryBinding>(FragmentDictionaryBinding::inflate),
    DictionaryAdapter.ListItemClickListener {

    private var viewModel: DictionaryViewModel? = null;

    private val adapter = DictionaryAdapter(this)

//    private val viewModel: DictionaryViewModel by viewModel()

    override fun init() {
        viewModel = DictionaryViewModel.getInstance(requireContext())
        viewModel!!.getAllWords()
        println("HELLO")
        observeLD()
        setListeners()
        initRecycler()
    }

    private fun observeLD() {
        viewModel?.dictLiveData?.observe(viewLifecycleOwner) {
            setUI(it)
        }
    }

    private fun setUI(data: List<WordDO>?) {
        data?.let {
            adapter.items = it as MutableList<WordDO>
            adapter.notifyDataSetChanged()
        }
    }

    private fun initRecycler() {
        binding.dictionaryRecycler.adapter = adapter
        binding.dictionaryRecycler.layoutManager = LinearLayoutManager(context)
    }

    private fun setListeners() = with(binding) {
        plusBtn.setOnClickListener {
            val word = binding.etWord.text.toString()
            val topic = binding.etTopic.text.toString()

            if (word.isEmpty() || topic.isEmpty()) {
                Toast.makeText(context, "Тема и слово должны быть прописаны", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }

            val unique = checkUnique(word, topic)
            if (unique) {
                val id = setAndGetID()
                val vm = viewModel
                viewModel?.insertWord(word, topic, id)
                Toast.makeText(context, "Слово $word добавлено", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, "Слово $word уже добавлено", Toast.LENGTH_SHORT).show()
            }
        }


    }


    private fun checkUnique(word: String, topic:String): Boolean {
        var unique = true
        viewModel?.dictLiveData?.value?.forEach {
            if (it.word.lowercase() == word.lowercase() && it.topic.lowercase() == topic.lowercase()) {
                unique = false
            }
        }
        return unique
    }

    override fun onListItemClick(clickedItemIndex: Int) {
        viewModel?.deleteWord(clickedItemIndex)
        val word = viewModel?.dictLiveData?.value?.get(clickedItemIndex)?.word
        Toast.makeText(context, "Слово $word удалено", Toast.LENGTH_SHORT).show()
    }

    private fun setAndGetID(): Int {
        val sharedPreference =
            requireActivity().getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        val id = sharedPreference.getInt("id", 0)
        val editor = sharedPreference.edit()
        editor.putInt("id", id + 1)
        editor.commit()
        return id;
    }

}
