package com.example.alias.ui.configure.dictionary

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.alias.room.WordRussianEntity
import com.example.alias.room.WordsDao
import com.example.alias.room.WordsDatabase
import com.example.alias.ui.configure.vm.ConfigureViewModel
import kotlinx.coroutines.*


class DictionaryViewModel constructor(private val wordsDao: WordsDao) :
    ViewModel() {

    private val _dictLiveData = MutableLiveData<List<WordDO>>()
    var dictLiveData: LiveData<List<WordDO>> = _dictLiveData

      fun getAllWords()  {
        viewModelScope.launch(Dispatchers.IO + handler) {

                kotlin.runCatching {
                    wordsDao.getAllRussianWords()
                }.onSuccess { wordsFromDB ->
                    val words = wordRussianEntityToWordDO(wordsFromDB)
                    _dictLiveData.postValue(words)
                }.onFailure {
                    println("smth in getAllWords went wrong ${it.message}")
                }

        }
    }

    fun insertWord(name: String, topic: String, id: Int){
        viewModelScope.launch(Dispatchers.IO) {
            kotlin.runCatching {
                wordsDao.insertRusWord(WordRussianEntity(id,name,topic))
            }.onSuccess {
                getAllWords()
            }.onFailure {
                println("INSERT smth in getAllWords went wrong ${it.message}")
            }
        }
    }

    fun deleteWord(index:Int){
        viewModelScope.launch(Dispatchers.IO + handler) {
            kotlin.runCatching {
                dictLiveData.value?.get(index)?.let { wordsDao.deleteByKeyword(it.word, it.topic) }
            }.onSuccess {
                getAllWords()
            }.onFailure {
                println("DELETE smth in getAllWords went wrong ${it.message}")
            }
        }
    }

    private fun wordRussianEntityToWordDO(entityList: List<WordRussianEntity>): List<WordDO> {
        val result = mutableListOf<WordDO>()
        entityList.forEach { word ->
            result.add(WordDO(word.name, word.topic))
        }

        return result
    }

    private val handler = CoroutineExceptionHandler { coroutineContext, throwable ->
        println("error occured $throwable on $coroutineContext")
    }

    companion object {

        @Volatile
        private var INSTANCE: DictionaryViewModel? = null

        fun getInstance(context: Context): DictionaryViewModel {
            return INSTANCE ?: DictionaryViewModel(  WordsDatabase
                .getInstance(
                    context.applicationContext
                )
                .wordsDao)
        }
    }

}
