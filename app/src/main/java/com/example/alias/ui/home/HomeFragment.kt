package com.example.alias.ui.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.alias.MainActivity
import com.example.alias.MainActivity.Companion.SHARED_PREFERENCE_NAME
import com.example.alias.R
import com.example.alias.databinding.FragmentHomeBinding
import com.example.alias.ui.base.BaseFragment
import com.example.alias.ui.configure.ConfigureFragmentDirections
import com.example.alias.util.LocaleUtils

class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {
    private var isSpinnerInitialized = false

    override fun init() {
        initSpinner()
        navigateToConfigurationFragment()
        navigateToRulesFragment()
//        chooseLanguage()

        binding.btn2.setOnClickListener{
            println("CLCI")
            binding.btn2.setBackgroundColor(R.color.white)
            findNavController().navigate(
                HomeFragmentDirections.actionHomeFragmentToDictionaryFragment())
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {}
    }

    private fun initSpinner() = with(binding) {
        val adapter = ArrayAdapter.createFromResource(requireContext(), R.array.language_array, R.layout.custom_spinner)
        adapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item)
//        chooseLang.adapter = adapter
//        chooseLang.setSelection(getSharedPreference().getInt(PREFERENCE_NAME, PREFERENCE_DEFAULT_VALUE))
    }

    private fun navigateToRulesFragment() {
        binding.rulesBtn.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_rulesDialogFragment)
        }
    }

    private fun navigateToConfigurationFragment() {
        binding.confBtn.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_configureFragment)
        }
    }


    private fun getSharedPreference() =
        requireActivity().getSharedPreferences(
            SHARED_PREFERENCE_NAME,
            Context.MODE_PRIVATE
        )

    fun setSharedPreference(language: Int) =
        getSharedPreference()
            .edit().putInt(PREFERENCE_NAME, language).apply()

    companion object {
        const val PREFERENCE_NAME = "language"
        const val PREFERENCE_DEFAULT_VALUE = 0
        private const val ZERO = 0
        private const val ONE = 1
        private const val TWO = 2
        private const val EN = "en"
        private const val RU = "ru"
        private const val KA = "ka"
    }

}
