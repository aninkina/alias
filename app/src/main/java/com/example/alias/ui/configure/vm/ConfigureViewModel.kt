package com.example.alias.ui.configure.vm

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.alias.room.WordRussianEntity
import com.example.alias.room.WordsDao
import com.example.alias.room.WordsDatabase
import com.example.alias.util.GameMode
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ConfigureViewModel constructor(private val wordsDao: WordsDao) : ViewModel() {


    private val _gameMode = MutableLiveData(GameMode())
    val gameMode: LiveData<GameMode>
        get() = _gameMode

    private var _viewPagerCurrentItem = MutableLiveData(0)

    val viewPagerCurrentItem: LiveData<Int>
        get() = _viewPagerCurrentItem

    val setViewPagerCurrentItem: (Int) -> Unit = { _viewPagerCurrentItem.postValue(it) }

    private val _gameModeComponents =
        MutableLiveData(Array(4) { false })

    val gameModeComponenets: LiveData<Array<Boolean>>
        get() = _gameModeComponents

    private val _topicLiveData = MutableLiveData<List<String>>()
    var topicLiveData: LiveData<List<String>> = _topicLiveData

    private val _topicIndex = MutableLiveData<Int>()
    var topicIndex: LiveData<Int> = _topicIndex

    private val _isTeamsInputValid = MutableLiveData(true)
    val isTeamsInputValid: LiveData<Boolean>
        get() = _isTeamsInputValid
    val handleIsTeamsInputValid: (Boolean) -> Unit = { _isTeamsInputValid.value = it }


    fun setGameMode(gameMode: GameMode){
        _gameMode.value = gameMode;
    }

    fun getAllTopics()  {
        viewModelScope.launch(Dispatchers.IO) {

            kotlin.runCatching {
                wordsDao.getAllRussianWords()
            }.onSuccess { wordsFromDB ->
                val topics = wordRussianEntityToTopic(wordsFromDB)
                _topicLiveData.postValue(topics.toList())
            }.onFailure {
                println("smth in getAllWords went wrong ${it.message}")
            }

        }
    }

    fun setTopicIndex(index:Int){
        _topicIndex.postValue(index)
    }

    private fun changedGameComponents(pos: Int): Array<Boolean> {
        val res = gameModeComponenets.value
        res!![pos] = true
        return res
    }

    fun setIsClassic(isClassic: Boolean) {
        _gameMode.value?.let {
            _gameMode.value = GameMode(
                isClassic = isClassic,
                teams = it.teams,
                timePerRound = it.timePerRound,
                pointsToWin = it.pointsToWin
            )
        }

        _gameModeComponents.value = changedGameComponents(ZERO)
    }

    fun setTeams(teams: MutableList<String>) {
        if (teams.size != teams.toSet().size)
            handleIsTeamsInputValid(false)
        else {
            handleIsTeamsInputValid(true)
            _gameMode.value?.let {
                _gameMode.value = GameMode(
                    isClassic = it.isClassic,
                    teams = teams,
                    timePerRound = it.timePerRound,
                    pointsToWin = it.pointsToWin
                )
                _gameModeComponents.value = changedGameComponents(ONE)
            }
        }

    }

    fun setTimePerRound(timePerRound: Int?) {
        _gameMode.value?.let {
            _gameMode.value = GameMode(
                isClassic = it.isClassic,
                teams = it.teams,
                timePerRound = timePerRound,
                pointsToWin = it.pointsToWin
            )
        }
        _gameModeComponents.value = changedGameComponents(TWO)

    }

    fun setPointsToWin(pointsToWin: Int?) {
        _gameMode.value?.let {
            _gameMode.value = GameMode(
                isClassic = it.isClassic,
                teams = it.teams,
                timePerRound = it.timePerRound,
                pointsToWin = pointsToWin
            )
        }
        _gameModeComponents.value = changedGameComponents(THREE)

    }

    private fun wordRussianEntityToTopic(entityList: List<WordRussianEntity>): HashSet<String> {
        val result = HashSet<String>()
        entityList.forEach { word ->
            result.add(word.topic)
        }

        return result
    }
    companion object {
        private const val ZERO = 0
        private const val ONE = 1
        private const val TWO = 2
        private const val THREE = 3
        private const val DEFAULT_SCORE = 30
        private const val DEFAULT_TIME = 100

//        @Volatile
//        private var INSTANCE: ConfigureViewModel? = null
//
//        fun getInstance(context: Context): ConfigureViewModel {
//            return INSTANCE ?: ConfigureViewModel(  WordsDatabase
//                .getInstance(
//                    context.applicationContext
//                )
//                .wordsDao)
//        }

        @Volatile private var INSTANCE: ConfigureViewModel ? = null
        fun  getInstance(context:Context): ConfigureViewModel {
            if(INSTANCE == null){
                synchronized(this) {
                    INSTANCE = ConfigureViewModel(WordsDatabase.getInstance(context.applicationContext).wordsDao)
                }
            }
            return INSTANCE!!
        }
    }


}

object ViewModel2 {
    init {
        // do your initialization stuff
    }
}