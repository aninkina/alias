package com.example.alias.di

import com.example.alias.room.WordsDatabase
import com.example.alias.ui.arcade.vm.ArcadeViewModel
import com.example.alias.ui.classic.vm.ClassicViewModel
import com.example.alias.ui.configure.dictionary.DictionaryViewModel
import com.example.alias.ui.configure.vm.ConfigureViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val wordsDao = module {
    single {
        WordsDatabase
            .getInstance(
                androidApplication()
                    .applicationContext
            )
            .wordsDao
    }
}

val viewModels = module {
    single { ClassicViewModel(get()) }
//    factory { ConfigureViewModel(get()) }
//    factory { DictionaryViewModel(get()) }
}


